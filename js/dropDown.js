var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, state, value, kind, f) {
    if (kind === "m") throw new TypeError("Private method is not writable");
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a setter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
    return (kind === "a" ? f.call(receiver, value) : f ? f.value = value : state.set(receiver, value)), value;
};
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, state, kind, f) {
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a getter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
    return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
};
var _DropDown_instances, _DropDown_cb, _DropDown_current, _DropDown_ul, _DropDown_button, _DropDown_prefix, _DropDown_optionId, _DropDown_currentLi, _DropDown_isOpen, _DropDown_openMenu, _DropDown_clicked, _DropDown_onBlur, _DropDown_onKeyDown, _DropDown_selectLi, _DropDown_closeMenu, _DropDown_onOutsideClick, _DropDown_onPopState, _DropDown_replaceState, _DropDown_pushState;
// Dropdown menu component.
//
export class DropDown {
    // Create an accessible dropdown. The container element should be an empty div with an id.
    // @param ul The ul element in which to build our drop down.
    // @param entries The text entries to add to the drop down.
    // @param cb      The callback to call when the entry is selected. This will receive the
    //                string entry and the MouseEvent object.
    //
    constructor(container, label, entries, cb) {
        _DropDown_instances.add(this);
        _DropDown_cb.set(this, void 0); // callback to notify when the selection has changed.
        _DropDown_current.set(this, void 0); // text of the selected option.
        _DropDown_ul.set(this, void 0);
        _DropDown_button.set(this, void 0);
        _DropDown_prefix.set(this, void 0); // The id of the container, used as prefix for other elements.
        if (entries.length < 2)
            throw "Trying to create dropdown with less then 2 entries";
        this.container = container;
        __classPrivateFieldSet(this, _DropDown_prefix, this.container.id, "f");
        if (__classPrivateFieldGet(this, _DropDown_prefix, "f") === "")
            throw "Dropdown container must have an id.";
        const base = `
			<span id="${__classPrivateFieldGet(this, _DropDown_prefix, "f")}--title">${label}</span>

			<button id="${__classPrivateFieldGet(this, _DropDown_prefix, "f")}--button" aria-haspopup="listbox" aria-labelledby="${__classPrivateFieldGet(this, _DropDown_prefix, "f")}--title ${__classPrivateFieldGet(this, _DropDown_prefix, "f")}--button"></button>

			<ul
				id              = "${__classPrivateFieldGet(this, _DropDown_prefix, "f")}--listbox"
				tabindex        = "-1"
				role            = "listbox"
				aria-labelledby = "${__classPrivateFieldGet(this, _DropDown_prefix, "f")}--title"
			></ul>
		`;
        this.container.innerHTML = base;
        __classPrivateFieldSet(this, _DropDown_button, container.querySelector("button"), "f");
        __classPrivateFieldSet(this, _DropDown_ul, container.querySelector("ul"), "f");
        __classPrivateFieldSet(this, _DropDown_cb, cb, "f");
        __classPrivateFieldSet(this, _DropDown_current, entries[0], "f");
        for (const e of entries) {
            const li = document.createElement("li");
            li.innerText = e;
            li.setAttribute("role", "option");
            li.setAttribute("aria-selected", "false");
            li.setAttribute("id", __classPrivateFieldGet(this, _DropDown_instances, "m", _DropDown_optionId).call(this, e));
            // Pass the string that is clicked.
            //
            li.addEventListener("click", __classPrivateFieldGet(this, _DropDown_instances, "m", _DropDown_clicked).bind(this, e));
            __classPrivateFieldGet(this, _DropDown_ul, "f").append(li);
        }
        __classPrivateFieldGet(this, _DropDown_button, "f").innerText = __classPrivateFieldGet(this, _DropDown_current, "f");
        __classPrivateFieldGet(this, _DropDown_button, "f").addEventListener("click", __classPrivateFieldGet(this, _DropDown_instances, "m", _DropDown_openMenu).bind(this));
        __classPrivateFieldGet(this, _DropDown_ul, "f").addEventListener("blur", __classPrivateFieldGet(this, _DropDown_instances, "m", _DropDown_onBlur).bind(this));
        this.container.addEventListener("keydown", __classPrivateFieldGet(this, _DropDown_instances, "m", _DropDown_onKeyDown).bind(this));
        __classPrivateFieldGet(this, _DropDown_instances, "m", _DropDown_replaceState).call(this);
        window.addEventListener("popstate", __classPrivateFieldGet(this, _DropDown_instances, "m", _DropDown_onPopState).bind(this));
        window.addEventListener("mousedown", __classPrivateFieldGet(this, _DropDown_instances, "m", _DropDown_onOutsideClick).bind(this), { capture: true });
    }
}
_DropDown_cb = new WeakMap(), _DropDown_current = new WeakMap(), _DropDown_ul = new WeakMap(), _DropDown_button = new WeakMap(), _DropDown_prefix = new WeakMap(), _DropDown_instances = new WeakSet(), _DropDown_optionId = function _DropDown_optionId(text) {
    return `${__classPrivateFieldGet(this, _DropDown_prefix, "f")}--option--${text}`;
}, _DropDown_currentLi = function _DropDown_currentLi() {
    return document.getElementById(__classPrivateFieldGet(this, _DropDown_instances, "m", _DropDown_optionId).call(this, __classPrivateFieldGet(this, _DropDown_current, "f")));
}, _DropDown_isOpen = function _DropDown_isOpen() {
    return this.container.classList.contains("dropdown__open");
}, _DropDown_openMenu = function _DropDown_openMenu() {
    this.container.classList.add("dropdown__open");
    __classPrivateFieldGet(this, _DropDown_button, "f").setAttribute("aria-expanded", "true");
    __classPrivateFieldGet(this, _DropDown_ul, "f").setAttribute("aria-activedescendant", __classPrivateFieldGet(this, _DropDown_instances, "m", _DropDown_optionId).call(this, __classPrivateFieldGet(this, _DropDown_current, "f")));
    __classPrivateFieldGet(this, _DropDown_instances, "m", _DropDown_currentLi).call(this).setAttribute("aria-selected", "true");
    __classPrivateFieldGet(this, _DropDown_ul, "f").focus();
}, _DropDown_clicked = function _DropDown_clicked(val /*, evt: MouseEvent */) {
    // close dropdown
    //
    __classPrivateFieldGet(this, _DropDown_instances, "m", _DropDown_selectLi).call(this, val);
    __classPrivateFieldGet(this, _DropDown_instances, "m", _DropDown_closeMenu).call(this);
}, _DropDown_onBlur = function _DropDown_onBlur() {
    __classPrivateFieldGet(this, _DropDown_instances, "m", _DropDown_closeMenu).call(this);
}, _DropDown_onKeyDown = function _DropDown_onKeyDown(evt) {
    switch (evt.key) {
        case "ArrowUp":
            {
                evt.preventDefault();
                if (!__classPrivateFieldGet(this, _DropDown_instances, "m", _DropDown_isOpen).call(this)) {
                    __classPrivateFieldGet(this, _DropDown_instances, "m", _DropDown_openMenu).call(this);
                    break;
                }
                const prev = __classPrivateFieldGet(this, _DropDown_instances, "m", _DropDown_currentLi).call(this).previousElementSibling;
                if (prev)
                    __classPrivateFieldGet(this, _DropDown_instances, "m", _DropDown_selectLi).call(this, prev.textContent);
                break;
            }
        case "ArrowDown":
            {
                evt.preventDefault();
                if (!__classPrivateFieldGet(this, _DropDown_instances, "m", _DropDown_isOpen).call(this)) {
                    __classPrivateFieldGet(this, _DropDown_instances, "m", _DropDown_openMenu).call(this);
                    break;
                }
                const next = __classPrivateFieldGet(this, _DropDown_instances, "m", _DropDown_currentLi).call(this).nextElementSibling;
                if (next)
                    __classPrivateFieldGet(this, _DropDown_instances, "m", _DropDown_selectLi).call(this, next.textContent);
                break;
            }
        case "Escape":
            evt.preventDefault();
            __classPrivateFieldGet(this, _DropDown_instances, "m", _DropDown_closeMenu).call(this);
            break;
        case "Enter":
            evt.preventDefault();
            if (__classPrivateFieldGet(this, _DropDown_instances, "m", _DropDown_isOpen).call(this))
                __classPrivateFieldGet(this, _DropDown_instances, "m", _DropDown_closeMenu).call(this);
            else
                __classPrivateFieldGet(this, _DropDown_instances, "m", _DropDown_openMenu).call(this);
            break;
    }
}, _DropDown_selectLi = function _DropDown_selectLi(val) {
    if (val === __classPrivateFieldGet(this, _DropDown_current, "f"))
        return;
    __classPrivateFieldGet(this, _DropDown_instances, "m", _DropDown_currentLi).call(this).removeAttribute("aria-selected");
    __classPrivateFieldSet(this, _DropDown_current, val, "f");
    __classPrivateFieldGet(this, _DropDown_instances, "m", _DropDown_currentLi).call(this).setAttribute("aria-selected", "true");
    __classPrivateFieldGet(this, _DropDown_button, "f").innerText = val;
    __classPrivateFieldGet(this, _DropDown_ul, "f").setAttribute("aria-activedescendant", __classPrivateFieldGet(this, _DropDown_instances, "m", _DropDown_optionId).call(this, val));
    __classPrivateFieldGet(this, _DropDown_instances, "m", _DropDown_pushState).call(this);
    __classPrivateFieldGet(this, _DropDown_cb, "f").call(this, val);
}, _DropDown_closeMenu = function _DropDown_closeMenu() {
    this.container.classList.remove("dropdown__open");
    __classPrivateFieldGet(this, _DropDown_button, "f").removeAttribute("aria-expanded");
    __classPrivateFieldGet(this, _DropDown_ul, "f").removeAttribute("aria-activedescendant");
    __classPrivateFieldGet(this, _DropDown_instances, "m", _DropDown_currentLi).call(this).removeAttribute("aria-selected");
    __classPrivateFieldGet(this, _DropDown_button, "f").focus();
}, _DropDown_onOutsideClick = function _DropDown_onOutsideClick(evt) {
    if (this.container.contains(evt.target) || !this.container.classList.contains("dropdown__open"))
        return;
    // stop other actions on click like opening the carousel.
    //
    window.addEventListener("click", function stopClick(e) { e.stopPropagation(); }, {
        once: true,
        capture: true,
    });
    __classPrivateFieldGet(this, _DropDown_instances, "m", _DropDown_closeMenu).call(this);
    evt.preventDefault();
}, _DropDown_onPopState = function _DropDown_onPopState(evt) {
    if (evt.state === null || typeof evt.state.dropdown === "undefined")
        return;
    __classPrivateFieldSet(this, _DropDown_current, evt.state.dropdown, "f");
}, _DropDown_replaceState = function _DropDown_replaceState() {
    const state = window.history.state || {};
    state.dropdown = __classPrivateFieldGet(this, _DropDown_current, "f");
    window.history.replaceState(state, document.title, "");
    return this;
}, _DropDown_pushState = function _DropDown_pushState() {
    const state = window.history.state || {};
    state.dropdown = __classPrivateFieldGet(this, _DropDown_current, "f");
    window.history.pushState(state, document.title, "");
    return this;
};
