var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, state, value, kind, f) {
    if (kind === "m") throw new TypeError("Private method is not writable");
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a setter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
    return (kind === "a" ? f.call(receiver, value) : f ? f.value = value : state.set(receiver, value)), value;
};
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, state, kind, f) {
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a getter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
    return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
};
var _Contact_instances, _Contact_firstFocusableElement, _Contact_focusableContent, _Contact_focusableElements, _Contact_form, _Contact_lastFocusableElement, _Contact_onKeyDown, _Contact_loopFocus;
// Manages the contact form on the profile page. We capture focus when it's open.
//
export class Contact {
    constructor(name) {
        _Contact_instances.add(this);
        _Contact_firstFocusableElement.set(this, void 0);
        _Contact_focusableContent.set(this, void 0);
        _Contact_focusableElements.set(this, void 0);
        _Contact_form.set(this, void 0);
        _Contact_lastFocusableElement.set(this, void 0);
        _Contact_onKeyDown.set(this, __classPrivateFieldGet(this, _Contact_instances, "m", _Contact_loopFocus).bind(this));
        __classPrivateFieldSet(this, _Contact_form, document.getElementById("contact--form"), "f");
        __classPrivateFieldSet(this, _Contact_focusableElements, "button, [href], input, select, textarea, [tabindex]:not([tabindex=\"-1\"])", "f");
        __classPrivateFieldSet(this, _Contact_focusableContent, __classPrivateFieldGet(this, _Contact_form, "f").querySelectorAll(__classPrivateFieldGet(this, _Contact_focusableElements, "f")), "f");
        __classPrivateFieldSet(this, _Contact_firstFocusableElement, __classPrivateFieldGet(this, _Contact_focusableContent, "f")[0], "f");
        __classPrivateFieldSet(this, _Contact_lastFocusableElement, __classPrivateFieldGet(this, _Contact_focusableContent, "f")[__classPrivateFieldGet(this, _Contact_focusableContent, "f").length - 1], "f");
        const openButton = document.getElementById("button--contact");
        const closeButton = document.getElementById("contact--close");
        const title = document.getElementById("contact--name");
        title.innerText = name;
        openButton.addEventListener("click", this.openDialog.bind(this));
        closeButton.addEventListener("click", this.closeDialog.bind(this));
        __classPrivateFieldGet(this, _Contact_form, "f").addEventListener("submit", this.submit.bind(this));
    }
    openDialog() {
        document.body.classList.add("body__contact");
        document.addEventListener("keydown", __classPrivateFieldGet(this, _Contact_onKeyDown, "f"));
        document.getElementById("prenom").focus();
    }
    closeDialog() {
        document.body.classList.remove("body__contact");
        document.removeEventListener("keydown", __classPrivateFieldGet(this, _Contact_onKeyDown, "f"));
        document.getElementById("button--contact").focus();
    }
    submit(evt) {
        evt.preventDefault();
        const prenom = __classPrivateFieldGet(this, _Contact_form, "f").elements.namedItem("prenom");
        const nom = __classPrivateFieldGet(this, _Contact_form, "f").elements.namedItem("nom");
        const email = __classPrivateFieldGet(this, _Contact_form, "f").elements.namedItem("email");
        const message = __classPrivateFieldGet(this, _Contact_form, "f").elements.namedItem("message");
        console.log("form submitted with:");
        console.log(" prenom:", prenom.value);
        console.log("    nom:", nom.value);
        console.log("  email:", email.value);
        console.log("message:", message.value);
        this.closeDialog();
    }
}
_Contact_firstFocusableElement = new WeakMap(), _Contact_focusableContent = new WeakMap(), _Contact_focusableElements = new WeakMap(), _Contact_form = new WeakMap(), _Contact_lastFocusableElement = new WeakMap(), _Contact_onKeyDown = new WeakMap(), _Contact_instances = new WeakSet(), _Contact_loopFocus = function _Contact_loopFocus(evt) {
    if (evt.key === "Escape") {
        this.closeDialog();
        return;
    }
    if (evt.key !== "Tab")
        return;
    // if shift key pressed for shift + tab combination on the first element.
    //
    if (document.activeElement === __classPrivateFieldGet(this, _Contact_firstFocusableElement, "f") && evt.shiftKey) {
        __classPrivateFieldGet(this, _Contact_lastFocusableElement, "f").focus();
        evt.preventDefault();
    }
    else if (document.activeElement === __classPrivateFieldGet(this, _Contact_lastFocusableElement, "f") && !evt.shiftKey) {
        __classPrivateFieldGet(this, _Contact_firstFocusableElement, "f").focus();
        evt.preventDefault();
    }
};
