var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, state, kind, f) {
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a getter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
    return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
};
var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, state, value, kind, f) {
    if (kind === "m") throw new TypeError("Private method is not writable");
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a setter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
    return (kind === "a" ? f.call(receiver, value) : f ? f.value = value : state.set(receiver, value)), value;
};
var _Media_instances, _Media_thumbnail, _Media_liked, _Media_onOpenCarousel, _Media_onLikesClicked, _Image_elem, _Video_elem, _Video_width, _Video_height;
import * as util from "./util.js";
export class Media {
    constructor(json) {
        _Media_instances.add(this);
        _Media_thumbnail.set(this, void 0);
        _Media_liked.set(this, false);
        this.id = json.id;
        this.photographerID = json.photographerID;
        this.title = json.title;
        this.tags = json.tags;
        this.likes = json.likes;
        this.date = json.date;
        this.price = json.price;
        this.image = json.image;
        this.video = json.video;
    }
    // Factory
    //
    static createMedia(json) {
        if (typeof json.image === "string") {
            return new Image(json);
        }
        else if (typeof json.video === "string") {
            return new Video(json);
        }
        else {
            throw "Invalid json data, no image, nor video";
        }
    }
    // Html to go into the gallery as thumbnail. Note that this.element() is part of this.
    //
    thumbnail() {
        if (typeof __classPrivateFieldGet(this, _Media_thumbnail, "f") !== "undefined")
            return __classPrivateFieldGet(this, _Media_thumbnail, "f");
        const html = `
			<figure class="thumb">
				<picture tabindex="0"></picture>
				<figcaption>
					<span class="title">${this.title}</span>
					<span class="likes">${this.likes}

					<div class="heart" tabindex="0" role="checkbox" aria-checked="false">
						<img src="img/icons/heart-solid.svg" alt="Liker l'image">
					</div>
				</figcaption>
			</figure>
		`;
        __classPrivateFieldSet(this, _Media_thumbnail, util.toDom(html), "f");
        const picture = __classPrivateFieldGet(this, _Media_thumbnail, "f").querySelector("picture");
        const heart = __classPrivateFieldGet(this, _Media_thumbnail, "f").querySelector(".heart");
        picture.append(this.element());
        picture.addEventListener("click", __classPrivateFieldGet(this, _Media_instances, "m", _Media_onOpenCarousel).bind(this));
        heart.addEventListener("click", __classPrivateFieldGet(this, _Media_instances, "m", _Media_onLikesClicked).bind(this, heart));
        return __classPrivateFieldGet(this, _Media_thumbnail, "f");
    }
}
_Media_thumbnail = new WeakMap(), _Media_liked = new WeakMap(), _Media_instances = new WeakSet(), _Media_onOpenCarousel = function _Media_onOpenCarousel() {
    const event = new CustomEvent("open-carousel", { detail: this });
    document.body.dispatchEvent(event);
}, _Media_onLikesClicked = function _Media_onLikesClicked(heart) {
    const likes = __classPrivateFieldGet(this, _Media_thumbnail, "f").querySelector(".likes");
    const num = likes.firstChild;
    let parsed = parseInt(num.textContent);
    if (__classPrivateFieldGet(this, _Media_liked, "f")) {
        --parsed;
        heart.setAttribute("aria-checked", "false");
    }
    else {
        ++parsed;
        heart.setAttribute("aria-checked", "true");
    }
    __classPrivateFieldSet(this, _Media_liked, !__classPrivateFieldGet(this, _Media_liked, "f"), "f");
    num.textContent = parsed.toString();
    console.assert(parsed >= 0);
    const event = __classPrivateFieldGet(this, _Media_liked, "f") ?
        new CustomEvent("likes-increment")
        : new CustomEvent("likes-decrement");
    document.body.dispatchEvent(event);
};
class Image extends Media {
    constructor(json) {
        super(json);
        _Image_elem.set(this, void 0);
        this.url = json.image;
    }
    element() {
        if (typeof __classPrivateFieldGet(this, _Image_elem, "f") !== "undefined")
            return __classPrivateFieldGet(this, _Image_elem, "f");
        const html = `<img class="media-content" src="img/${this.photographerID}/${this.url}" alt="${this.title}, closeup view">`;
        __classPrivateFieldSet(this, _Image_elem, util.toDom(html), "f");
        return __classPrivateFieldGet(this, _Image_elem, "f");
    }
    carousel() {
        const img = this.element().cloneNode(true);
        // Remove ", closeup view"
        //
        img.setAttribute("alt", this.title);
        return img;
    }
    naturalHeight() {
        const img = this.element();
        return img.naturalHeight;
    }
    naturalWidth() {
        const img = this.element();
        return img.naturalWidth;
    }
}
_Image_elem = new WeakMap();
class Video extends Media {
    constructor(json) {
        super(json);
        _Video_elem.set(this, void 0);
        // we buffer this because on back button popstate on video, the video isn't always loaded when
        // we need to know it's size, in which case it will return 0.
        //
        _Video_width.set(this, void 0);
        _Video_height.set(this, void 0);
        this.url = json.video;
    }
    element() {
        if (typeof __classPrivateFieldGet(this, _Video_elem, "f") !== "undefined")
            return __classPrivateFieldGet(this, _Video_elem, "f");
        const html = `
			<video class="media-content" tabindex="-1">
				<source src="img/${this.photographerID}/${this.url}" type="video/mp4">
				Your browser does not support the video tag.
			</video>
		`;
        __classPrivateFieldSet(this, _Video_elem, util.toDom(html), "f");
        return __classPrivateFieldGet(this, _Video_elem, "f");
    }
    carousel() {
        const video = this.element().cloneNode(true);
        video.setAttribute("controls", "");
        video.setAttribute("tabindex", "0");
        return video;
    }
    naturalHeight() {
        if (typeof __classPrivateFieldGet(this, _Video_height, "f") !== "undefined")
            return __classPrivateFieldGet(this, _Video_height, "f");
        const vid = this.element();
        __classPrivateFieldSet(this, _Video_height, vid.videoHeight, "f");
        return __classPrivateFieldGet(this, _Video_height, "f");
    }
    naturalWidth() {
        if (typeof __classPrivateFieldGet(this, _Video_width, "f") !== "undefined")
            return __classPrivateFieldGet(this, _Video_width, "f");
        const vid = this.element();
        __classPrivateFieldSet(this, _Video_width, vid.videoWidth, "f");
        return __classPrivateFieldGet(this, _Video_width, "f");
    }
}
_Video_elem = new WeakMap(), _Video_width = new WeakMap(), _Video_height = new WeakMap();
