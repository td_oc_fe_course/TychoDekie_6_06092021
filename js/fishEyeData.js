import { Media } from "./media.js";
import { Photographer } from "./photographer.js";
export class FishEyeData {
    constructor(json) {
        this.photographers = [];
        this.media = [];
        for (const p of json.photographers) {
            this.photographers.push(new Photographer(p));
        }
        for (const m of json.media) {
            this.media.push(Media.createMedia(m));
        }
        Object.freeze(this);
    }
    allTags() {
        let all = [];
        for (const p of this.photographers) {
            all = all.concat(p.tags);
        }
        return new Set(all.sort());
    }
    findPhotographer(id) {
        return this.photographers.find(p => p.id == id);
    }
    mediaByAuthor(p) {
        return this.media.filter(m => m.photographerID == p.id);
    }
}
