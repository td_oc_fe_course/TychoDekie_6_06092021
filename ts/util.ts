import { Media } from "./media.js";

export function toDom( input: string ) : HTMLElement
{
	const template = document.createElement( "template" );
	template.innerHTML = input;

	return template.content.firstElementChild as HTMLElement;
}



export function mediaArrayEquals( a1: Media[], a2: Media[] ) : boolean
{
	if( a1.length != a2.length ) return false;

	let i = a1.length;

	while( i-- )
	{
	   if( a1[i]!.id !== a2[i]!.id ) return false;
	}

	return true
}
