export class Router
{
	page: "index" | "profile";

	constructor()
	{
		     if( document.body.id === "page-index"   ) this.page = "index"  ;
		else if( document.body.id === "page-profile" ) this.page = "profile";

		else throw "Router, could not find which page we are running. Body must have";
	}


	getQueryVar( name: string ) : string | null
	{
		const params = new URLSearchParams( document.location.search.substring(1) );

		return params.get( name );
	}
}
