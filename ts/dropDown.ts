// Dropdown menu component.
//
export class DropDown
{
	#cb      : (val: string) => void                  ; // callback to notify when the selection has changed.
	#current : string                                 ; // text of the selected option.
	container: HTMLDivElement                         ;
	#ul      : HTMLUListElement                       ;
	#button  : HTMLButtonElement                      ;
	#prefix  : string                                 ; // The id of the container, used as prefix for other elements.


	// Create an accessible dropdown. The container element should be an empty div with an id.
	// @param ul The ul element in which to build our drop down.
	// @param entries The text entries to add to the drop down.
	// @param cb      The callback to call when the entry is selected. This will receive the
	//                string entry and the MouseEvent object.
	//
	constructor( container: HTMLDivElement, label: string, entries: string[], cb: (val: string) => void )
	{
		if ( entries.length < 2 ) throw "Trying to create dropdown with less then 2 entries";

		this.container = container;
		this.#prefix   = this.container.id;

		if( this.#prefix === "" ) throw "Dropdown container must have an id.";


		const base =
		`
			<span id="${this.#prefix}--title">${label}</span>

			<button id="${this.#prefix}--button" aria-haspopup="listbox" aria-labelledby="${this.#prefix}--title ${this.#prefix}--button"></button>

			<ul
				id              = "${this.#prefix}--listbox"
				tabindex        = "-1"
				role            = "listbox"
				aria-labelledby = "${this.#prefix}--title"
			></ul>
		`;

		this.container.innerHTML = base;

		this.#button   = container.querySelector( "button" )!;
		this.#ul       = container.querySelector( "ul"     )!;
		this.#cb       = cb;
		this.#current  = entries[0]!;


		for( const e of entries )
		{
			const li = document.createElement( "li" );

			li.innerText = e;
			li.setAttribute( "role"         , "option"          );
			li.setAttribute( "aria-selected", "false"           );
			li.setAttribute( "id"           , this.#optionId(e) );

			// Pass the string that is clicked.
			//
			li.addEventListener( "click", this.#clicked.bind(this, e) );

			this.#ul.append( li );
		}


		this.#button  .innerText = this.#current;
		this.#button  .addEventListener( "click"  , this.#openMenu .bind(this) );
		this.#ul      .addEventListener( "blur"   , this.#onBlur   .bind(this) );
		this.container.addEventListener( "keydown", this.#onKeyDown.bind(this) );

		this.#replaceState();
		window.addEventListener( "popstate" , this.#onPopState    .bind(this)                  );
		window.addEventListener( "mousedown", this.#onOutsideClick.bind(this), {capture: true} );
	}



	#optionId( text: string ) : string
	{
		return `${this.#prefix}--option--${text}`;
	}


	#currentLi() : HTMLElement
	{
		return document.getElementById( this.#optionId( this.#current ) )!;
	}


	#isOpen() : boolean
	{
		return this.container.classList.contains( "dropdown__open" );
	}



	// Open the menu.
	//
	#openMenu() : void
	{
		this.container.classList.add( "dropdown__open" );
		this.#button.setAttribute( "aria-expanded", "true" );

		this.#ul.setAttribute( "aria-activedescendant", this.#optionId(this.#current) );
		this.#currentLi().setAttribute( "aria-selected", "true" );
		this.#ul.focus();
	}



	// Handle the click on our list elements.
	//
	// Verify whether the ul was open or not:
	// - if it was closed, open it.
	// - if it was open:
	//   1. close it
	//   2. put the right li at the top
	//   3. call callback if new value is different than the old.
	//
	#clicked( val: string /*, evt: MouseEvent */ ) : void
	{
		// close dropdown
		//
		this.#selectLi( val );

		this.#closeMenu();
	}



	#onBlur() : void
	{
		this.#closeMenu();
	}



	#onKeyDown( evt: KeyboardEvent ) : void
	{
		switch( evt.key )
		{
			case "ArrowUp":
			{
				evt.preventDefault();

				if( !this.#isOpen() )
				{
					this.#openMenu();
					break;
				}

				const prev = this.#currentLi().previousElementSibling;
				if( prev ) this.#selectLi( prev.textContent! );
				break;
			}

			case "ArrowDown":
			{
				evt.preventDefault();

				if( !this.#isOpen() )
				{
					this.#openMenu();
					break;
				}

				const next = this.#currentLi().nextElementSibling;
				if( next ) this.#selectLi( next.textContent! );
				break;
			}


			case "Escape":

				evt.preventDefault();
				this.#closeMenu();
				break;


			case "Enter":

				evt.preventDefault();

				if( this.#isOpen() ) this.#closeMenu();
				else                 this.#openMenu();

				break;
		}
	}



	#selectLi( val: string ) : void
	{
		if( val === this.#current ) return;

		this.#currentLi().removeAttribute( "aria-selected" );
		this.#current = val;
		this.#currentLi().setAttribute( "aria-selected", "true" );
		this.#button.innerText = val;
		this.#ul.setAttribute( "aria-activedescendant", this.#optionId(val) )

		this.#pushState();
		this.#cb( val );
	}



	#closeMenu() : void
	{
		this.container.classList.remove( "dropdown__open" );

		this.#button     .removeAttribute( "aria-expanded"         );
		this.#ul         .removeAttribute( "aria-activedescendant" );
		this.#currentLi().removeAttribute( "aria-selected"         );

		this.#button.focus();
	}



	// Event listener that closes the dropdown when a click happens outside of it.
	//
	#onOutsideClick( evt: Event ) : void
	{
		if( this.container.contains(evt.target as Node) || !this.container.classList.contains("dropdown__open") ) return;

		// stop other actions on click like opening the carousel.
		//
		window.addEventListener( "click",

			function stopClick(e) { e.stopPropagation(); },

			{
				once   : true,
				capture: true,
			}
		);


		this.#closeMenu();
		evt.preventDefault();
	}



	#onPopState( evt: PopStateEvent ) : void
	{
		if( evt.state === null || typeof evt.state.dropdown === "undefined" ) return;

		this.#current = evt.state.dropdown;
	}



	#replaceState() : DropDown
	{
		const state    = window.history.state || {};
		state.dropdown = this.#current;

		window.history.replaceState( state, document.title, "" );

		return this;
	}



	#pushState() : DropDown
	{
		const state    = window.history.state || {};
		state.dropdown = this.#current;

		window.history.pushState( state, document.title, "" );

		return this;
	}
}
