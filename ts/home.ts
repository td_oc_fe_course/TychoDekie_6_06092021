import { FishEyeData  } from "./fishEyeData.js";
import { TagCloud     } from "./tagCloud.js";
import { Router       } from "./router.js";



export class Home
{
	#contenuLink: HTMLElement;
	#data       : FishEyeData;
	#tagCloud   : TagCloud   ; // the one at the top.
	#router     : Router     ;



	constructor( data: FishEyeData, router: Router )
	{
		this.#data   = data;
		this.#router = router;
		const nav    = document.querySelector ( "header > nav" ) as HTMLElement;


		this.#tagCloud = new TagCloud( Array.from(data.allTags()) );
		nav.append( this.#tagCloud.html( "Filtrer les photographes par le mot clef: " ) );

		// https://github.com/microsoft/TypeScript/issues/28357#issuecomment-711131035
		//
		window.addEventListener( "filter-change", { handleEvent: this.#onFilter.bind(this) } );

		this.#generateSummaries();


		const filter = this.#filterQuery();

		if( filter ) this.#tagCloud.toggle( filter );


		this.#contenuLink = document.getElementById( "contenu-link" )!;

		window.addEventListener( "scroll", this.#onScroll  .bind(this) );
	}



	// Adds the summaries to the home page.
	//
	#generateSummaries() : void
	{
		const prof = document.getElementById( "profiles" ) as HTMLElement;

		for( const p of this.#data.photographers )
		{
			prof.append( p.profileSummary() );
		}
	}



	// Applies the filters, hiding photographers that don't have media with hidden filters.
	//
	#onFilter( evt: CustomEvent< Map<string, boolean> > ) : void
	{
		const filters = evt.detail;
		let   showAll = false;


		// if every filter is unset, consider them all set.
		//
		if( [...filters.values()].every( elem => !elem ) )

			showAll = true
		;


		const prof = document.getElementById( "profiles" ) as HTMLElement;

		outer: for( const p of this.#data.photographers )
		{
			const div = prof.querySelector( `.profile-${p.id}` )!;


			if( !showAll )
			{
				for( const [tag, filterActive] of filters! )
				{
					if( filterActive && !p.tags.includes(tag) )
					{
						div.classList.add( "hide" );
						continue outer;
					}
				}
			}


			div.classList.remove( "hide" );
		}
	}



	// Show the "contenu" link when scrolling down.
	//
	#onScroll() : void
	{
		document.documentElement.scrollTop > 50 /*pixels*/ ?

			  this.#contenuLink.classList.remove( "hide" )
			: this.#contenuLink.classList.add   ( "hide" )
		;
	}



	#filterQuery() : string | null
	{
		const filter = this.#router.getQueryVar( "filter" );

		if( filter === null || filter === "" ) return null;

		const tags = [...this.#data.allTags()];


		if( !tags.includes(filter) )

			throw `Trying to filter on unknown filter: ${filter}`
		;


		return filter;
	}
}

